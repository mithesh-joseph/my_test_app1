class Address {
  String firstLane;
  String secondLane;

  Address(this.firstLane, this.secondLane);

  factory Address.fromJson(Map<String, dynamic> map) {
    return Address(
      map['firstLane'],
      map['secondLane'],
    );
  }

  Map<String, dynamic> toJson() {
    var map = {
      'firstLane': firstLane,
      "secondLane": secondLane,
    };
    return map;
  }
}
