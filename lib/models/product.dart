import 'address.dart';
import '../utils/parser/parser.dart';

class Product {
  String id;
  String name;
  String description;
  int price;
  String image;
  Address address;

  Product(this.id, this.name, this.address, this.description, this.price, this.image);

  factory Product.fromJson(Map<String, dynamic> data) {
    return Product(
      data['id'],
      data['name'],
      Address.fromJson(data['address']),
      data['description'],
      data['price'],
      data['image'],
    );
  }

  Map<String, dynamic> toJson() {
    var map = {
      'id':id,
      'name': name,
      "description": description,
      'price': price,
      'image': image,
    };
    if (address != null) {
      map['address'] = Parser().toJson(address);
    }
    return map;
  }
}
