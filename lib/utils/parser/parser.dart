import 'dart:convert';

class Parser {

  ///Parse json method
  dynamic parseJsonStringFrom(String inputString) {
    dynamic obj = jsonDecode(inputString);
    return obj;
  }

  ///Convert an object to JsonString
  String toJson(dynamic input) {
    dynamic jsonString = jsonEncode(input);
    return jsonString;
  }
}