import 'package:flutter/material.dart';
import '../modules/startup/startup.dart';
import '../manager/app_manager.dart';

void main() async {
  final user = await AppManager.instance.initialize();
  return runApp(StartupView());
}