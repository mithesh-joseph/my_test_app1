import 'package:flutter/material.dart';
import 'package:my_test_app1/manager/product_manager.dart';
import 'package:my_test_app1/models/product.dart';
import 'package:provider/provider.dart';

class SignUpView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignUpViewState();
  }
}

class SignUpViewState extends State<SignUpView> with ChangeNotifier {
  List<Product> _products;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      left: true,
      top: true,
      right: true,
      bottom: true,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: _buildAppBar(),
        body: _mainContainer(),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('White Rabbit SignUp'),
    );
  }

  Widget _mainContainer() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.grey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: Text('Get data'),
            onPressed: () async {
              await ProductManager().fetchProducts();
            },
          ),
          Container(
            height:_products.length*50.0,
            child: ListView.builder(
              reverse: true,
              itemCount: _products.length,
              itemBuilder: (context, index) {
                return Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: _listViewTile(_products[index]));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _listViewTile(Product product) {
    return Container(
      height: 40,
      color: Colors.green,
      child: Text(product.name),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    print('didChangeDependencies');

    final products =
        Provider.of<ProductManager>(context).getProductsFromCache();
    if (products != null) {
      _products = products;
    } else {
      Future.delayed(
        Duration(milliseconds: 500),
        () {
          //_showNoProductAvailableAlert();
        },
      );
    }
  }
}
