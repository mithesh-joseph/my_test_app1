import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../manager/product_manager.dart';
import '../../utils/app_config.dart';
import '../../manager/app_manager.dart';
import '../../utils/page_route.dart' as PR;
import '../../modules/home/home.dart';
import '../../modules/signup/signup.dart';

class StartupView extends StatefulWidget {
  StartupView();

  @override
  State<StatefulWidget> createState() {
    return StartupViewState();
  }
}

class StartupViewState extends State<StartupView> {
  @override
  void initState() {
    super.initState();
    AppManager.instance.setInitialRoute(PR.signIn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProductManager>.value(
          value: ProductManager(),
        )
      ],
      child: MaterialApp(
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child);
        },
        title: AppConfig.appName,

        /// instead of conventional '/' root [AppManager]
        /// is expected to provide initial route based on the configured
        /// routes from the values of [routes]
        initialRoute: AppManager.instance.initialRoute,

        routes: {
          PR.signIn: (context) => SignUpView(),
          PR.home: (context) => HomeView(),
        },
        theme: ThemeData.fallback(),
      ),
    );
  }
}
