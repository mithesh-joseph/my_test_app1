import 'package:my_test_app1/models/product.dart';
import 'entity_repository.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'sql_data_context.dart';

class ProductRepository extends EntityRepository<Product> {
  /// Database context is as singleton instance of SQLiteDataContext
  Future<Database> get database async {
    return SQLiteDataContext().database;
  }

  @override
  String entityId(Product entity) => entity.id;

  @override
  Product fromJson(Map map) => Product.fromJson(map);

  @override
  String get table => 'Product';

  @override
  Map toJson(Product entity) => entity.toJson();

  @override
  Future<bool> insert(List<Product> entities) async {
    // Return early
    if (entities == null || entities.isEmpty) return Future.value(false);

    try {
      return _insertProducts(entities);
    } catch (e) {
      throw ('Unable to save the products. data invalid');
    }
  }

  /// Insert Conversations
  Future<bool> _insertProducts(List<Product> products) async {
    // Return early
    if (products == null && products.isEmpty) return Future.value(false);

    // database
    final db = await database;

    // a transaction
    var batch = db.batch();

    for (var product in products) {
      _insertProduct(product, batch);
    }

    // committing the transaction
    var result = await batch.commit(noResult: true);
    return result != null ? true : false;
  }

  /// Insert Product one by one
  ///
  /// Uses a batch to commit all at once
  void _insertProduct(Product product, Batch batch) {
    // Insert just the conversation
    batch.insert('Product', product.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
//
//    if (conversation.messages != null) {
//      for (MessageDAO message in conversation.messages) {
//        // incase to be on a safer side
//        message.conversationId = conversation.id;
//        batch.insert('Message', message.toJson(),
//            conflictAlgorithm: ConflictAlgorithm.replace);
//      }
//    }
//
//    if (conversation.members != null) {
//      for (ConversationUserDAO conversationUser in conversation.members) {
//        // incase to be on a safer side
//        conversationUser.conversationId = conversation.id;
//
//        /// For current user update the last read message time stamp
//        /// and the unread count
//        if (conversationUser.userId == conversation.userId) {
//          /// Get the unread messages and cache for faster access
//          conversationUser.unreadMessages =
//              conversation.calculateUnreadMessageCount();
//
//          /// Set the last message id
//          conversationUser.lastMessageId = conversation.messages.last.messageId;
//
//          /// This is needed to reduce the unread message fetch time
//          /// Need to confirm if this needs to be updated
//          final messageId = conversationUser.lastReadMessageId;
//          if (messageId != null && messageId.isNotEmpty) {
//            final message = conversation.messageWithId(messageId);
//            conversationUser.lastReadMessageTimestamp = message?.createdAt;
//          }
//        }
//
//        batch.insert(
//          'ConversationUser',
//          conversationUser.toJson(),
//          conflictAlgorithm: ConflictAlgorithm.replace,
//        );
//      }
//    }
//
//    if (conversation.reminders != null) {
//      for (Reminder reminder in conversation.reminders) {
//        // incase to be on a safer side
//        reminder.conversationId = conversation.id;
//        batch.insert('Reminder', reminder.toJson(),
//            conflictAlgorithm: ConflictAlgorithm.replace);
//      }
//    }
  }
}
