
/// A generic repository for performing CRUD on data store
abstract class IRepository<T> {

  Future<bool> insert(List<T> entities);

//  /// Find all entities with the raw sql query
//  /// By providing this we could minimize the
//  /// creation of new custom repositories
//  Future<List<T>> find(String query, [List<dynamic> queryArgs]);
//
//  /// Find one by identifier
//  Future<T> findOne(String entityId);

  /// Update multiple entities
  Future<bool> update(List<T> entities);

  /// Delete a list of entities
  Future<bool> delete(List<T> entities);

  // Delete one entity
  Future<bool> deleteOne(String entityId);

  /// Clear all table records
  Future<int> truncate();
}