import 'package:flutter/foundation.dart';

import '../models/product.dart';
import '../database/product_repository.dart';
import '../services/product_service.dart';

class ProductManager with ChangeNotifier {
  ///Creating singleton instance for the ProductManager
  static final ProductManager __sharedInstance = ProductManager.internal();

  factory ProductManager() {
    return __sharedInstance;
  }

  ProductManager.internal();

  // Cache
  final _cache = Map<String, Product>();

  final _repository = ProductRepository();

  /// Fetching Products form api and parse the result ot the Product Model.
  Future<void> fetchProducts() async {
    var products = await ProductService().fetchProductsFromApi();
    if (products != null && products.isNotEmpty) {
      await _processEntities(products);
    }
    /// Add to cache
    _setProducts(products);
  }

  /// Process the data received by api request and
  /// update to database
  Future<void> _processEntities(List<Product> products) async {
    if (products == null || products.isEmpty) return;
    await _processRecords(products);
  }

  Future<void> _processRecords(List<Product> items) async {
    if (items == null) return;
    await _repository.insert(items);
  }

  /// Clear and repopulate cache
  _setProducts(List<Product> products, {bool notify = true}) {
    _cache.clear();
    products.forEach((c) {
      _cache[c.id] = c;
    });

    _notifyChanges(notify: notify);
  }

  /// Notify the changes to the listeners
  _notifyChanges({bool notify = true}) {
    /// Get al the cached products
    final products = _cache.values.toList();

    if (notify) {
      notifyListeners();
    }
  }

  /// Check if a conversation exists
  List<Product> getProductsFromCache() {
    /// Check in cache first
    return _cache != null ? _cache.values.toList() : null;
  }
}
