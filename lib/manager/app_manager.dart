import '../utils/page_route.dart' as AppRoute;

abstract class AppManager {
  static AppManager _sharedInstance;

  /// Get a shared instance
  static AppManager get instance {
    _sharedInstance ??= _AppManagerImpl();
    return _sharedInstance;
  }

  /// Intialize app
  Future<void> initialize();

  /// Get the initial route of the app
  String get initialRoute;

  void setInitialRoute(String route);
}

class _AppManagerImpl implements AppManager {
  /// The initial route of the app
  String _initialRoute = AppRoute.signIn;

  @override
  String get initialRoute => _initialRoute;

  @override
  Future<void> initialize() async {

  }

  @override
  void setInitialRoute(String route) {
    _initialRoute = route;
  }

  Future<void> _initializeAppRouter() {
    bool isUserActive = true;
    if (isUserActive) {
      _initialRoute = AppRoute.home;
    } else {
      _initialRoute = AppRoute.signIn;
    }
  }
}
