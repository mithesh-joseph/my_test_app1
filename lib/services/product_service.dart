import 'package:http/http.dart' as http;
import '../models/product.dart';
import '../utils/parser/parser.dart';

class ProductService {

  Future<List<Product>> fetchProductsFromApi() async {

    final requestUrl = _getRequestUrl();

    try {
      final response = await http
          .get(requestUrl, headers: {'content-type': 'application/json'});
      if (response.statusCode == 200) {
        var products = response.body;
        return products != null ? _parseProducts(products) : List<Product>();
      } else {
        /// Here based on the error code (response.statusCode), we need to handle
        /// the error message. For the time being it is just thrown.
        throw ("Error occured: ${response.statusCode}" + response.reasonPhrase);
      }
    } catch (exception) {
      throw ("Exception occurred " + exception.toString());
    }
  }

  /// Parse response body and convert it to Product object
  List<Product> _parseProducts(String responseBody) {
    List<dynamic> parsedData = Parser().parseJsonStringFrom(responseBody);

    ///We need to parse the data to the corresponding objects.
    var result = parsedData != null && parsedData.isNotEmpty
        ? parsedData.map((product) => _toProduct(product)).toList()
        : [];
    return result;
  }

  String _getRequestUrl() {
    return 'https://run.mocky.io/v3/fd8da71e-9a63-4437-acc9-d8ab6fc0fafa';
  }

  /// converting each products to Models
  Product _toProduct(Map<String, dynamic> map) {
    return Product.fromJson(map);
  }
}
